import unittest
def add(a, b):
	return a + b


def substract(a, b):
	return a - b


def multiply(a, b):
	return a * b


def divide(numerator, denominator):
	return float(numerator) / denominator


class Test(unittest.TestCase):
	def test(self):
		self.assertEqual(2+3,add(2,3))
		self.assertEqual(2-3,substract(2,3))
		self.assertEqual(2*3,multiply(2,3))
		self.assertEqual(2/3,divide(2,3))

matest = Test()
matest.test()
