import math
import unittest

class Point:
    '''constructeur d’un point à partir de ses coordonnées cartésiennes'''
    def __init__(self, x, y):
        self.x = x
        self.y = y
        self.r = math.sqrt(x**2 + y**2)
        self.t = math.atan2(y,x)

    def x(self):
        '''renvoient les coordonnéescartésiennes du point'''
        return self.x

    def y(self):
        '''renvoient les coordonnéescartésiennes du point'''
        return self.y

    def __str__(self):
        '''renvoie une expression textuelle dupoint, comme ​"​(2.0, 3.0)​"'''
        return '({0}, {1})'.format(float(self.x), float(self.y))

    def __eq__(self, autrePoint):
        ''' ​a​ et ​b​ représentent-ils deuxpoints ​égaux​ ? '''
        try:
            return bool(self.x == autrePoint.x and self.y == autrePoint.y)
        except AttributeError:
            print('{0} n\'est pas comparable a {1}'.format(autrePoint, self))

    def homothetie(self, k):
        '''applique au point unehomothétie de centre (0, 0) et de rapport ​k'''
        self.x = k *self.x
        self.y = k *self.y


    def translation(self, dx, dy):
        '''applique au pointune translation de vecteur (​dx​, ​dy​)'''
        self.x = self.x + dx
        self.y = self.y + dy


class LignePol:
    '''représenterdetelsobjets.Les sommets y seront mémorisés sous forme de liste​ de points'''
    def __init__(self, *arg):
        for ar in arg:
            if not isinstance(ar, Point):
                raise Exception('Merci de fournir uniquement des points')
        self.sommets = [*arg]
        self.nb_sommets = len(self.sommets)
        print(*arg)

    def get_sommet(self, i):
        '''renvoie le i​ème​ sommet'''
        return str(self.sommets[i])

    def set_sommet(self, i, p):
        '''donne le point ​ppour valeur du i​ème​ sommet'''
        if isinstance(p, Point):
            self.sommets[i] = p
        else:
            Exception('Merci de fournir des points')

    def __str__(self):
        '''renvoie une expression de laligne polygonale sous forme de texte'''
        return 'Je suis une ligne composé des points : {}'.format([str(i) for i in self.sommets])

    def homothetie(self, k):
        '''applique à chaquesommet de la ligne polygonale une homothétie
        decentre (0, 0) et de rapport ​k'''
        for i in self.sommets:
            i.homothetie(k)

    def translation(self, dx, dy):
        '''applique àchaque sommet de la ligne polygonale unetranslation de vecteur (​dx​, ​dy​)'''
        for i in self.sommets:
            i.translation(dx, dy)





class Pile:
    '''implementation d'une pile'''
    def __init__(self):
        self.pile = []

    def empiler(self, i):
        '''Ajoute a la fin'''
        self.pile.append(i)

    def depiler(self, i=1):
        '''supprime les i elements specifie a la fin de la pile'''
        if len(self.pile) ==0:
            raise IndexError('Pas assez d\'element dans la file')
        else:
            self.pile = self.pile[:-i]

    def __str__(self):
        return str(self.pile)

    def sommet(self):
        if len(self.pile) ==0:
            raise Exception('La pile est vide')
        return self.pile[-1]

class File:
    '''implementation d'une file'''
    def __init__(self):
        self.file = []

    def enfiler(self, i):
        '''ajout au debut'''
        self.file.insert(0, i)

    def defiler(self):
        '''supprime dernier element'''
        self.file = self.file[:-1]

class RandomTest(unittest.TestCase):

    def test_depiler(self):
        """on ne peut dépiler une pile vide"""
        self.assertRaises(IndexError,Pile().depiler)

    def test_sommet_vide(self):
        """on ne peut lire le sommet d’une pile vide"""
        self.assertRaises(Exception,Pile().sommet)

    def test_vide_create(self):
        """une pile venant d’être créée est vide"""
        self.assertEqual(0,len(Pile().pile))

    def test_non_empty(self):
        """toute pile sur laquelle on vient d’empiler un élément est non vide"""
        a = Pile()
        a.empiler(5)
        self.assertNotEqual(0,len(a.pile))

    def inchange(self):
        """une pile qui subit un empilement suivi d’un dépilement est inchangée"""
        p = Pile()
        p.empiler(5)
        p.empiler(3)
        p.depiler()
        a = Pile()
        a.empiler(5)
        self.assertEqual(a.pile ,p.pile)

    def tete(self):
        """le sommet d’une pile sur laquelle on vient d’empiler un élément e est e."""
        p = Pile()
        p.empiler(3)
        self.assertEqual(p.sommet(),3)

    def tete_enfiler(self):
        """Si une file n’est pas vide, alors la tête d’une file sur
        laquelle on enfile un élément e est identique à latête de la file avant
        l’opération d’enfilement"""
        p = File()
        p.enfiler(3)
        p.enfiler(5)
        self.assertEqual(p.file[0],5)

    def test_vide_enfilement(self):
        """Une file vide qui subit un enfilement suivi d’un défilement est toujours une file vide"""
        p = File()
        p.enfiler(3)
        p.defiler()
        self.assertEqual(0,len(p.file))

    def test_enfilement_defilement(self):
        """Sur une file non vide, un enfilement suivi d’un défilement est
        identique à un défilement suivi d’unenfilement"""
        p = File()
        p.enfiler(3)
        p.enfiler(5)
        p.defiler()
        f = File()
        f.enfiler(3)
        f.defiler()
        f.enfiler(5)
        self.assertEqual(p.file,f.file)

RandomTest().test_depiler()
RandomTest().test_sommet_vide()
RandomTest().test_vide_create()
RandomTest().test_non_empty()

RandomTest().inchange()
RandomTest().tete()
RandomTest().tete_enfiler()
RandomTest().test_vide_enfilement()
RandomTest().test_enfilement_defilement()
